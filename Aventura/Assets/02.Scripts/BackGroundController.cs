﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundController : MonoBehaviour
{
    public float move;
    public float limit_pos;
    RectTransform rect_t;

    void Start()
    {
        rect_t = GetComponent<RectTransform>();
    }

    void FixedUpdate()
    {
        transform.Translate(move, 0, 0);

        if(rect_t.anchoredPosition.x <= limit_pos)
        {
            rect_t.anchoredPosition = new Vector2(limit_pos * -1, rect_t.anchoredPosition.y);
        }
    }
}
