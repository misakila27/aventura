﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndSceneManager : MonoBehaviour
{

    public GameObject storePanel;
    public GameObject mainPanel;

    public GameObject optionBar;
    public GameObject achieveScrollView;
    public GameObject treasureScrollView;

    public Toggle soundToggle;
    public Toggle musicToggle;
    public Toggle achieveToggle;

    public Text endScore;
    public Text endDay;
    private ResourceLoad resourceLoad;
    GameObject Obj_ResourceLoad;

    

    private InGameUiControl inGameUiControl;
    GameObject obj_UiManager;


    // Start is called before the first frame update
    void Start()
    {
       
        Obj_ResourceLoad = GameObject.Find("ResourceLoad");
        resourceLoad = Obj_ResourceLoad.GetComponent<ResourceLoad>();

        obj_UiManager = GameObject.Find("UIManager");
        inGameUiControl = obj_UiManager.GetComponent<InGameUiControl>();

        endDay.text = "Day - " +inGameUiControl.DayCount.ToString();
        endScore.text = "Score -" + inGameUiControl.scoreCount.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OptionButton()
    {
        if (optionBar.activeSelf == false)
        {
            optionBar.SetActive(true);
        }
        else
        {
            optionBar.SetActive(false);
        }

    }

    public void StoreButton()
    {
        storePanel.SetActive(true);
        mainPanel.SetActive(false);
    }

    public void StoreExitButton()
    {
        storePanel.SetActive(false);
        mainPanel.SetActive(true);
    }

    public void Achieve()
    {

        achieveScrollView.SetActive(true);
        treasureScrollView.SetActive(false);


    }

    public void Treasure()
    {
        achieveScrollView.SetActive(false);
        treasureScrollView.SetActive(true);
    }

    public void Done()
    {
        SceneManager.LoadScene("MainScene");
    }
}
