﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InGameUiControl : MonoBehaviour
{

    public GameObject volumeOn, volumeOff;
    public GameObject musicOn, musicOff;
    public Image hp;
    public Image mental;
    public Image supply;
    public int DayCount;
    public int scoreCount;
    public Text Day;
    public Text Score;
    private bool isSound, isMusic; // 사운드 체크
    private bool isMenu; // 메뉴
    public GameObject textPanel, menuPanel;
    public GameObject[] character = new GameObject[9];
    public GameObject[] usingCharacter = new GameObject[3];
    private ResourceLoad resourceLoad;
    GameObject Obj_ResourceLoad;
    public GameObject UIManger;
    public DialogueManager dialogueManager;
    
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(UIManger);
        Obj_ResourceLoad = GameObject.Find("ResourceLoad");
        resourceLoad = Obj_ResourceLoad.GetComponent<ResourceLoad>();
        
        CharacterAni1(resourceLoad);
        CharacterAni2(resourceLoad);
        CharacterAni3(resourceLoad);
      

        isMusic = true;
        isSound = true;
        isMenu = false;
        ShowDay();

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            Die();
        }
       
    }

    void CharacterAni1(ResourceLoad re)
    {
        switch (re.slot1Character)
        {
            case 1:
                   Instantiate(character[0],new Vector3(1,-3,0), Quaternion.identity);
                break;
            case 2:
                   Instantiate(character[1], new Vector3(1, -3, 0), Quaternion.identity);
                break;
            case 3:
                Instantiate(character[2], new Vector3(1, -3, 0), Quaternion.identity);
                break;
            case 4:
                Instantiate(character[3], new Vector3(1, -3, 0), Quaternion.identity);
                break;
            case 5:
                Instantiate(character[4], new Vector3(1, -3, 0), Quaternion.identity);
                break;
            case 6:
                Instantiate(character[5], new Vector3(1, -3, 0), Quaternion.identity);
                break;
            case 7:
                Instantiate(character[6], new Vector3(1, -3, 0), Quaternion.identity);
                break;
            case 8:
                Instantiate(character[7], new Vector3(1, -3, 0), Quaternion.identity);
                break;
            case 9:
                Instantiate(character[8], new Vector3(1, -3, 0), Quaternion.identity);
                break;
            default:
                break;

        }
    }
    void CharacterAni2(ResourceLoad re)
    {
        switch (re.slot2Character)
        {
            case 1:
                Instantiate(character[0], new Vector3(0, -3, 0), Quaternion.identity);
                break;
            case 2:
                Instantiate(character[1], new Vector3(0, -3, 0), Quaternion.identity);
                break;
            case 3:
                Instantiate(character[2], new Vector3(0, -3, 0), Quaternion.identity);
                break;
            case 4:
                Instantiate(character[3], new Vector3(0, -3, 0), Quaternion.identity);
                break;
            case 5:
                Instantiate(character[4], new Vector3(0, -3, 0), Quaternion.identity);
                break;
            case 6:
                Instantiate(character[5], new Vector3(0, -3, 0), Quaternion.identity);
                break;
            case 7:
                Instantiate(character[6], new Vector3(0, -3, 0), Quaternion.identity);
                break;
            case 8:
                Instantiate(character[7], new Vector3(0, -3, 0), Quaternion.identity);
                break;
            case 9:
                Instantiate(character[8], new Vector3(0, -3, 0), Quaternion.identity);
                break;
            default:
                break;

        }
    }


    void CharacterAni3(ResourceLoad re)
    {
        switch (re.slot3Character)
        {
            case 1:
                Instantiate(character[0], new Vector3(-1, -3, 0), Quaternion.identity);
                break;
            case 2:
                Instantiate(character[1], new Vector3(-1, -3, 0), Quaternion.identity);
                break;
            case 3:
                Instantiate(character[2], new Vector3(-1, -3, 0), Quaternion.identity);
                break;
            case 4:
                Instantiate(character[3], new Vector3(-1, -3, 0), Quaternion.identity);
                break;
            case 5:
                Instantiate(character[4], new Vector3(-1, -3, 0), Quaternion.identity);
                break;
            case 6:
                Instantiate(character[5], new Vector3(-1, -3, 0), Quaternion.identity);
                break;
            case 7:
                Instantiate(character[6], new Vector3(-1, -3, 0), Quaternion.identity);
                break;
            case 8:
                Instantiate(character[7], new Vector3(-1, -3, 0), Quaternion.identity);
                break;
            case 9:
                Instantiate(character[8], new Vector3(-1, -3, 0), Quaternion.identity);
                break;
            default:
                break;

        }
    }


    void ShowDay() // 게임 시작 시 한번 실행
    {
        Day.text = " D+" + DayCount.ToString();
    }  

    public void PlusDay() // 날짜가 지날 때 마다 실행 
    {
        DayCount++;
        scoreCount += DayCount;

        Score.text = "Score \n" + scoreCount.ToString();
        Day.text = " D+" + DayCount.ToString();
        if(DayCount % 11 == 0)
        {
            for(int i =1; i<20; i++)
            {
                dialogueManager.dialogueCheck[i] = false;
            }
        }
    }

    public void ChangeSound()
    {
      
        if (isSound == true)
        { 
    
            volumeOff.SetActive(true);
            isSound = false;
            //사운드 관련 추가
        }
        else
        {
            volumeOff.SetActive(false);
            isSound = true;
        }

    }

    public void ChangeMusic()
    {
        Debug.Log("ㅁㄴㅇㅁㄴㅇ");
        if (isMusic == true)
        {

            musicOff.SetActive(true);
            isMusic = false;
            //사운드 관련 추가
        }
        else
        {
            musicOff.SetActive(false);
            isMusic = true;
        }

    }

    public void MenuOn()
    {
        if(isMenu == false)
        {
            menuPanel.SetActive(true);
            textPanel.SetActive(false);
            isMenu = true;
            
        }
        else
        {
            menuPanel.SetActive(false);
            textPanel.SetActive(true);
            isMenu = false;
        }
    }

    public void MenuOff()
    {
        Debug.Log("눌렸다");
            menuPanel.SetActive(false);
            textPanel.SetActive(true);
         
    }


    public void MinusHealth(float minusValue)
    {
        hp.fillAmount -= minusValue * Time.deltaTime;
    }

  public  void MinusMental(float MinusValue)
    {
        mental.fillAmount -= MinusValue * Time.deltaTime;
    }

   public void MinusSupply(float MinusValue)
    {
        supply.fillAmount -= MinusValue * Time.deltaTime;
    }

    public void PlusHealth(float PlusValue)
    {
        
        hp.fillAmount += PlusValue * Time.deltaTime;
    }

    public void PlusMental(float PlusValue)
    {
        mental.fillAmount += PlusValue * Time.deltaTime;
    }

    public void PlusSupply(float PlusValue)
    {
        supply.fillAmount += PlusValue * Time.deltaTime;
    }

    public void LoadMap()
    {
        SceneManager.LoadScene("Map");
    }

    public void Die()
    {
      
            SceneManager.LoadScene("EndScene");
    

    }
    
    public void SaveExit()
    {

        SceneManager.LoadScene("MainScene");
    }

    public void GiveUp()
    {
        DestroyAllGameObjects();
        SceneManager.LoadScene("EndScene");

    }

    public void DestroyAllGameObjects()
    {
        GameObject[] GameObjects = (FindObjectsOfType<GameObject>() as GameObject[]);

        for (int i = 0; i < GameObjects.Length; i++)
        {
            Destroy(GameObjects[i]);
        }
    }

}
