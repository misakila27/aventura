﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainSceneUiControl : MonoBehaviour
{
    public GameObject cloud;
    float x;
    float y;
    bool down;
    public float time;
    // Start is called before the first frame update
    void Start()
    {
        time = 0;
        down = true;
        StartCoroutine(Timer());


    }

    void MoveCloud()
    {
        if (down == true)
        {
            y = 0.10f;
            down = false;
           
        }
        else
        {
            y = -0.10f;
            down = true;
         
       
        }
   

    }
    IEnumerator Timer()
    {
       if(time >2)
        {
         
            yield return new WaitForSeconds(2.0f);
        }
        Timer();

    }

    


        // Update is called once per frame
        void Update()
    {
        time += Time.deltaTime;
        if(time>2)
        {
            y = 0.001f;
            cloud.transform.Translate(new Vector3(-0.01f, y, cloud.transform.position.z));
            if(time >4)
            {
                time = 0;
            }
        }
        else
        {
            y = -0.001f;
            cloud.transform.Translate(new Vector3(-0.01f, y, cloud.transform.position.z));
        }

        if (cloud.transform.position.x <-7)
        {
            
            cloud.transform.position = new Vector3(7, cloud.transform.position.y,cloud.transform.position.z);
        }
          

    }


}
