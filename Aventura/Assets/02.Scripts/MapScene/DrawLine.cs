﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLine : MonoBehaviour
{
    private LineRenderer line;
    public GameObject characterTeam;
   public  MapControl mapControl;

    

    // Start is called before the first frame update
    void Start()
    {

        line = GetComponent<LineRenderer>();
        line.material.mainTextureScale = new Vector2(10,10);
        line.SetColors(Color.red, Color.yellow);

        line.SetWidth(0.1f, 0.1f);



    
    //    line.startColor = Color.red;
      //  line.endColor = Color.yellow;
        //line.startWidth = 0.1f;
        //line.endWidth = 0.1f;
    }

    // Update is called once per frame
    void Update()
    {
        InitLine();
    }

    public void InitLine()
    {
        line.SetPosition(0, new Vector3(characterTeam.transform.position.x, characterTeam.transform.position.y, -1));
        line.SetPosition(1, mapControl.Flag.transform.position);

    }

}
