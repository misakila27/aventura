﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapControl : MonoBehaviour
{
    private Transform target;
    public Camera mainCamera;
    Vector3 mousePosition;
    public int zoomCount;
    public GameObject characterTeam;
    
    public GameObject Flag;
    public DialogueManager dialogueManager;
    public GameObject map1, map2, map3;


    // Start is called before the first frame update
    void Start()
    {
        // line = GetComponent<LineRenderer>();
       
        zoomCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(1))
        {
            InitFlag();
            //    MovePosition();

        }
        if(Input.GetKeyDown(KeyCode.G))
        {
            MoveCharacter();
        }
       
   
    }

 
    public void InitFlag()
    {
        mousePosition = Input.mousePosition;
        mousePosition = mainCamera.ScreenToWorldPoint(mousePosition);
        Destroy(Flag);
        Flag = Instantiate(Flag,new Vector3(mousePosition.x,mousePosition.y, 0), Quaternion.identity);
   
    }

   
 

    private void DrawLine()
    {

        Vector3 lineStart = characterTeam.transform.position;
        Vector3 LineEnd = mousePosition;

    }

    public void MoveCharacter()
    {
        characterTeam.transform.position = Vector3.MoveTowards(characterTeam.transform.position,Flag.transform.position, 1f);
        if (dialogueManager == true)
        {

        }
        //characterTeam.transform.position = Vector3.Lerp(new Vector3(characterTeam.transform.position.x, characterTeam.transform.position.y, 0), new Vector3(mousePosition.x, mousePosition.y, 0), 0.001f);   
    }
       
    
   
    public void Zoom()
    {
        if(zoomCount<2)
        zoomCount++;
        switch (zoomCount)
        {
            case 1:
                map1.SetActive(false);
                map2.SetActive(true);
                map3.SetActive(false);
                break;
            case 2:
                map1.SetActive(false);
                map2.SetActive(false);
                map3.SetActive(true);
                //     mainCamera.orthographicSize = 5;
                break;
            case 3:
                map1.SetActive(false);
                map2.SetActive(false);
                map3.SetActive(true);
                //  mainCamera.orthographicSize = 2;
                break;
            default:
                break;

        }
               
    }

    public void ZoomOut()
    {
        if(zoomCount>0)
        zoomCount--;
        switch (zoomCount)
        {
            case 0:
                map1.SetActive(true);
                map2.SetActive(false);
                map3.SetActive(false);
                //  mainCamera.orthographicSize = 10;
                break;
            case 1:
                map1.SetActive(false);
                map2.SetActive(true);
                map3.SetActive(false);
                // mainCamera.orthographicSize = 7;
                break;
            case 2:
              //  mainCamera.orthographicSize = 5;
                break;
           
            default:
                break;

        }
    }

    public void Exit()
    {
        SceneManager.LoadScene("InGameScene");
    }
    //    public void MovePosition() // 맵에서 위치 변경 
    //{



    //        mousePosition = Input.mousePosition;
    //        mousePosition = mainCamera.ScreenToWorldPoint(mousePosition);
    //        Debug.Log(mousePosition);
    //    //      mainCamera.transform.position = new Vector3(mousePosition.x, mousePosition.y, -10);
    //    // 마우스로 찍은 위치의 좌표 값을 가져온다
    //    InitFlag();


    //    switch (zoomCount)
    //    {
    //        case 0:
    //            {
    //                mainCamera.transform.position = new Vector3(0f, 0f, -10);
    //                break;
    //            }
    //        case 1: //1.5 - 1.5 1 -3
    //            {
    //                if(mousePosition.x < -1.5)
    //                {
    //                    Debug.Log("1");
    //                    mainCamera.transform.position = new Vector3(-1.5f, mousePosition.y, -10);
    //                    mousePosition.x = -1.5f;
    //                }
    //                if (mousePosition.x > 1.5)
    //                {
    //                    Debug.Log("2");
    //                    mainCamera.transform.position = new Vector3(1.5f, mousePosition.y, -10);
    //                    mousePosition.x = 1.5f;

    //                }
    //                if (mousePosition.y < -3f)
    //                {
    //                    Debug.Log("3");
    //                    mainCamera.transform.position = new Vector3(mousePosition.x, -3f, -10);
    //                    mousePosition.y = -3f;
    //                }
    //                if (mousePosition.y > 1f)
    //                {
    //                    Debug.Log("4");
    //                    mainCamera.transform.position = new Vector3(mousePosition.x, 1f, -10);
    //                    mousePosition.y = 1f;
    //                }
    //                else
    //                {
    //                    mainCamera.transform.position = new Vector3(mousePosition.x, mousePosition.y, -10);
    //                }
    //                break;
    //            }
    //        case 2: //-2.75 2.75 3 -5 
    //            {
    //                if (mousePosition.x < -2.75f)
    //                {
    //                    mainCamera.transform.position = new Vector3(-2.75f, mousePosition.y, -10);
    //                    mousePosition.x = -2.75f;
    //                }
    //                if (mousePosition.x > 2.75f)
    //                {
    //                    mainCamera.transform.position = new Vector3(2.75f, mousePosition.y, -10);
    //                    mousePosition.x = 2.75f;
    //                }
    //                if (mousePosition.y < -5f)
    //                {
    //                    mainCamera.transform.position = new Vector3(mousePosition.x, -5f, -10);
    //                    mousePosition.y = -5f;
    //                }
    //                if (mousePosition.y > 3f)
    //                {
    //                    mainCamera.transform.position = new Vector3(mousePosition.x, 3f, -10);
    //                    mousePosition.y = 3f;
    //                }
    //                else
    //                {
    //                    mainCamera.transform.position = new Vector3(mousePosition.x, mousePosition.y, -10);
    //                }
    //                break;
    //            }

    //        case 3:     // -4 에서 4까지 6 8 

    //            {
    //                if (mousePosition.x < -4f)
    //                {
    //                    mainCamera.transform.position = new Vector3(-2.75f, mousePosition.y, -10);
    //                    mousePosition.x = -4f;
    //                }
    //                if (mousePosition.x > 4f)
    //                {
    //                    mainCamera.transform.position = new Vector3(2.75f, mousePosition.y, -10);
    //                    mousePosition.x = 4f;
    //                }
    //                if (mousePosition.y < -8f)
    //                {
    //                    mainCamera.transform.position = new Vector3(mousePosition.x, -5f, -10);
    //                    mousePosition.y = -8f;
    //                }
    //                if (mousePosition.y > 6f)
    //                {
    //                    mainCamera.transform.position = new Vector3(mousePosition.x, 3f, -10);
    //                    mousePosition.y = 6;
    //                }
    //                else
    //                {
    //                    mainCamera.transform.position = new Vector3(mousePosition.x, mousePosition.y, -10);
    //                }
    //                break;
    //            }

    //    }

    //}

}

