﻿using UnityEngine;

using System.Collections;



public class DrawLineTest : MonoBehaviour

{

    private LineRenderer lineRenderer;



    // Use this for initialization

    void Start()

    {

        //라인렌더러 설정

        lineRenderer = GetComponent<LineRenderer>();

        lineRenderer.SetColors(Color.red, Color.yellow);

        lineRenderer.SetWidth(0.1f, 0.1f);



        //라인렌더러 처음위치 나중위치

        lineRenderer.SetPosition(0, transform.position);

        lineRenderer.SetPosition(1, transform.position + new Vector3(0, 10, 0));



    }



    // Update is called once per frame

    void Update()

    {

    }

}


