﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharacterButtonManager : MonoBehaviour
{


    public GameObject mainPanel, characterStorePanel, clothesWindowPanel, accessoryWindowPanel, flagWindowPanel, coinWindowPanel, itemWindowPanel, exitWindowPanel;

    public Button setup, music, sound, character1Select, character2Select, character3Select, itemSelect, coin, buyPoint, clothes, accessory,
        flag, cancel, confirm, exitCharacterStore, closeWindow, 
        Supply, minusSupply,plusHealth, minusHealth, plusMental, minusMental, characterSet, yes, no;

    GameObject[] checkPanel = new GameObject[6];

    // Start is called before the first frame update
    void Start()
    {
      
        checkPanel[0] = clothesWindowPanel;
        checkPanel[1] = accessoryWindowPanel;
        checkPanel[2] = flagWindowPanel;
        checkPanel[3] = coinWindowPanel;
        checkPanel[4] = itemWindowPanel;
        checkPanel[5] = exitWindowPanel;
    }

    // Update is called once per frame
    void Update() // 캐릭터 메인만 켜져있을 때 
    {
        if (mainPanel.activeSelf == true && characterStorePanel.activeSelf == false && clothesWindowPanel.activeSelf == false && accessoryWindowPanel.activeSelf == false && 
            flagWindowPanel.activeSelf == false && coinWindowPanel.activeSelf == false && itemWindowPanel.activeSelf == false && exitWindowPanel.activeSelf == false)
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                exitWindowPanel.SetActive(true);
            }
        }


    }
   
    public void MainExit()
    {
       
    }

    public void SetUpButton()
    {
        sound.gameObject.SetActive(true);
        music.gameObject.SetActive(true);
    }

    public void MusicButton()
    {

    }
    
    public void SoundButton()
    {

    }
    public void Character1SelectButton()
    {
        characterStorePanel.SetActive(true);
        mainPanel.SetActive(false);
    }
    public void Character2SelectButton()
    {
        characterStorePanel.SetActive(true);
        mainPanel.SetActive(false);
    }
    public void Character3SelectButton()
    {
        characterStorePanel.SetActive(true);
        mainPanel.SetActive(false);
    }

   

    public void ItemSelectButton()
    {
        itemWindowPanel.SetActive(true);
    }
    public void CoinButton()
    {

    }
   


    public void BuyPointButton()
    {

    }
        public void ClothesButton()
    {
        clothesWindowPanel.SetActive(true);
    }
        public void AccessoryButton()
    {
        accessoryWindowPanel.SetActive(true);
    }
        public void FlagButton()
    {
        flagWindowPanel.SetActive(true);
    }
        public void CancelButton()
    {

    }
        public void ConfirmButton()
    {
   
    }
        public void ExitCharacterStoreButton()
    {
        characterStorePanel.SetActive(false);
        mainPanel.SetActive(true);
    }
        public void CloseWindowButton()
    {
        for (int i = 0; i < 6; i++)
        {
            if (checkPanel[i].activeSelf == true)
            {
                checkPanel[i].SetActive(false);
            }
        }

    }


    public void PlusSupplyButton()
    {

    }
    public void MinusSupplyButton()
    {

    }

    public void PlusHealthButton()
    {

    }
    public void MinusHealthButton()
    {

    }
    public void PlusMentalButton()
    {

    }
    public void MinusMentalButton()
    {

    }
    public void CharacterSetButton()
    {
        SceneManager.LoadScene("InGameScene");
    }
    public void YesButton()
    {
        SceneManager.LoadScene("MainScene");
    }
    public void NoButton()
    {
        exitWindowPanel.SetActive(false);
    }
 

}
