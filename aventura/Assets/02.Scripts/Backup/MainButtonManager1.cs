﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainButtonManager : MonoBehaviour
{


    //배열로 GameObject를 만들어서 관리하는 방법을 알아봐야 할 듯 



    // CharacterStorePanel, StorePanel, MainPanel, ExitWindowPanel, AchieveScrollView ;

   // public GameObject[] panels;
     public GameObject characterStorePanel, storePanel, mainPanel, exitWindowPanel, achieveScrollView,coinWindowPanel,introWindowPanel;

    public Button artifactWindow, achieveWindow, exitStore, coin, setup, music, sound, yes, no, intro;


    //public GameObject[] panels;
    void Start()
    {
    
    //panels[0] = characterStorePanel;
    //panels[1] = storePanel;
    //panels[2] = mainPanel;
    //panels[3] = exitWindowPanel;
    //panels[4] = achieveScrollView;
    //panels[5] = coinWindowPanel;
    //panels[6] = introWindowPanel;

    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("asdsad");
        }
    }


    
   

    //배열로 만들어서 상태를 받은다음에 



    public void StartButton()
    {
        SceneManager.LoadScene("CharacterSelectScene");
    }

    public void StoreButton()
    {
        storePanel.SetActive(true);
        mainPanel.SetActive(false);
      
        
    }

    public void ExitButton() //현재 활성화 되어있는 Panel 을 받아 그 상태에 맞는 경로로 이동을 하게 게임오브젝트들의 액티브를 확인 후 액티브 되어 있는게 있으면 빠져나와서 그 내용의 EXIT 가 실행되게   
    {
        
        if(achieveScrollView.activeSelf == true) //이런식으로 if문 노가다로 구현은 가능하겠지만 별로 좋지 않은 방법 인듯 
        {
            achieveScrollView.SetActive(false);
        }
        else if(storePanel.activeSelf == true)
        {
            storePanel.SetActive(false);
            mainPanel.SetActive(true);
        }
        if (coinWindowPanel.activeSelf == true)
        {
            coinWindowPanel.SetActive(false);
        }
    }

    public void CoinButton()
    {
        coinWindowPanel.SetActive(true);

    }


    

    public void GameExit()
    {
        if (mainPanel.activeSelf == true)
        {
            exitWindowPanel.SetActive(true);
            mainPanel.SetActive(false);
        }
    }

    public void SetUpButton()
    {
        sound.gameObject.SetActive(true);
        music.gameObject.SetActive(true);
    }

    public void SoundButton()
    {
        
    }

    public void MusicButton()
    {

    }

    public void ArtifactButton()
    {
        achieveScrollView.SetActive(false);
    }

    public void AchieveButton()
    {
        achieveScrollView.SetActive(true);

       // ArtifactWindowButton.interactable = false; 뒤에 있는 버튼이 눌려도 될듯 업적이 켜진 상태에서 



    }

    public void YesButton()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBPLAYER
         Application.OpenURL(webplayerQuitURL);
#else
         Application.Quit();
#endif

    }

    public void NoButton()
    {
        mainPanel.SetActive(true);
        exitWindowPanel.SetActive(false);

    }
    

    public void IntroButton()
    {
        if(introWindowPanel.activeSelf == true)
        {
            introWindowPanel.SetActive(false);
            coin.interactable = true;
            setup.interactable = true;
            music.interactable = true;
            sound.interactable = true;

        }
        else if(introWindowPanel.activeSelf == true && Input.GetKey(KeyCode.Escape))
        {
            introWindowPanel.SetActive(false);
            coin.interactable = true;
            setup.interactable = true;
            music.interactable = true;
            sound.interactable = true;

        }
        else
        {
            introWindowPanel.SetActive(true);
            coin.interactable = false;
            setup.interactable = false;
            music.interactable = false;
            sound.interactable = false;
        }

    }
}
