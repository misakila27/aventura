﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class OnGameSaveData : MonoBehaviour
{
    public int character1ID;
    public int character2ID;
    public int character3ID;
    public float positionX;
    public float positionY;
    public int day;
    public bool night;
    public float score;
    public float supply;
    public float mental;
    public float health;


    public static OnGameSaveData Instance;


    private void Awake()
    {
        if (Instance == null)
        {
            Debug.LogError("istance 가 없음 ");
        }
        Instance = this;


        if (System.IO.File.Exists(Path.Combine(Application.dataPath, "onGameSaveData.json")))
        {
            string onGameDataPath = Path.Combine(Application.dataPath, "onGameSaveData.json");
            string jsonData = File.ReadAllText(onGameDataPath);
            JsonUtility.FromJsonOverwrite(jsonData, Instance);
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
