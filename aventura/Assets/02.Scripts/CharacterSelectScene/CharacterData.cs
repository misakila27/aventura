﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterData : MonoBehaviour
{
   
    public CharacterList character;
    public Image image;
    private Text textUI;
    
    


    
    //CharaterData에 쓰이는 변수들 모음 
    #region 

    public int id;
    public string characterName;
    public int route;
    public string dialogue;
    public int clothesID;
    public int toolD;
    public int flagID;
    public float supply;
    public float mental;
    public float health;
    public bool lcok;
    public bool use;

    #endregion 

    private static CharacterData _instance = null;

    public static CharacterData Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new CharacterData();

            }
            return _instance;
        }
    }

    public void CharatcerImage(CharacterList character)
    {
  
    }


    public void SetCharacter(CharacterList character)
    {
        this.character = character;

        
        if(character == null)
        {
            image.enabled = false;

            gameObject.name = "Empty";
        }
        else
        {
            image.enabled = true;

           // gameObject.name = character.text;
            image.sprite = character.sprite;

        }

    }









    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
