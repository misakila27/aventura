﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterList {

    public string text;
    public Sprite sprite;
    public bool choice = false;
}

