﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class CharacterSelectSceneButton : MonoBehaviour
{
    public GameObject mainPanel;
    public GameObject characterSelectPanel;
    public GameObject coverPanel;
    public GameObject itemPanel;
    public GameObject itemScrollView;
    public GameObject clothesScrollView;
    public GameObject toolScrollView;
    public GameObject flagScrollView;

    public GameObject coinPanel;
    public GameObject optionBar;
    public GameObject optionBarInCharacter;

    public Toggle soundToggle;
    public Toggle musicToggle;
    public Toggle soundToggleInCharacter;
    public Toggle musicToggleInCharacter;

    public ResourceLoad resourceLoad;

    GameObject cos_flag;
    GameObject cos_tool;

    private static CharacterSelectSceneButton _instance = null;


    public Image characterImage;




    private void Start()
    {
        resourceLoad = GameObject.Find("ResourceLoad").GetComponent<ResourceLoad>();
    }



    public static CharacterSelectSceneButton Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new CharacterSelectSceneButton();

            }
            return _instance;
        }
    }


    public void ExitButton()
    {
        SceneManager.LoadScene("MainScene");
    }

    public void DoneButton()
    {
        SceneManager.LoadScene("Map");
    }

    public void OptionButton()
    {
        if (mainPanel.activeSelf)
        {
            if (optionBar.activeSelf == false)
            {
                optionBar.SetActive(true);
            }
            else
            {
                optionBar.SetActive(false);
            }
        }
        else if (characterSelectPanel.activeSelf)
        {
            if (optionBarInCharacter.activeSelf == false)
            {
                optionBarInCharacter.SetActive(true);
            }
            else
            {
                optionBarInCharacter.SetActive(false);
            }
        }
    }

    public void SoundToggle()
    {
        if (mainPanel.activeSelf)
        {
            if (soundToggle.isOn)
            {

            }
            else
            {

            }
        }
        else if (characterSelectPanel.activeSelf)
        {
            if (soundToggleInCharacter.isOn)
            {

            }
            else
            {

            }
        }        
    }

    public void MusicToggle()
    {
        if (mainPanel.activeSelf)
        {
            if (musicToggle.isOn)
            {

            }
            else
            {

            }
        }
        else if (characterSelectPanel.activeSelf)
        {
            if (musicToggleInCharacter.isOn)
            {

            }
            else
            {

            }
        }
    }

    public void SelectCharacter(int characterValue)
    {
       if(characterValue == 1)
        {
         //   characterImage = 
        }
      
     }



    public void CharacterButton()
    {
        mainPanel.SetActive(false);
        characterSelectPanel.SetActive(true);
    }

    public void CharacterCloseButton()
    {

        cos_flag = GameObject.Find("cos_flag");
        Destroy(cos_flag);
        cos_tool = GameObject.Find("cos_tool");
        Destroy(cos_tool);

        characterSelectPanel.SetActive(false);
        mainPanel.SetActive(true);
    }

    public void ItemButton()
    {
        coverPanel.SetActive(true);
        itemPanel.SetActive(true);
        itemScrollView.SetActive(true);
    }

    public void ClothesButton()
    {
        coverPanel.SetActive(true);
        itemPanel.SetActive(true);

        coverPanel.SetActive(true);
        itemPanel.SetActive(true);
       // clothesScrollView.SetActive(true);
        toolScrollView.SetActive(false);
        flagScrollView.SetActive(false);


        switch (resourceLoad.characterChoice)
        {
            case 1:
                clothesScrollView.SetActive(true);
                break;


        }
    }

    public void ToolButton()
    {
        coverPanel.SetActive(true);
        itemPanel.SetActive(true);
       
        clothesScrollView.SetActive(false);
        toolScrollView.SetActive(true);
        flagScrollView.SetActive(false);
    }

    public void FlagButton()
    {
        coverPanel.SetActive(true);
        itemPanel.SetActive(true);
        clothesScrollView.SetActive(false);
        toolScrollView.SetActive(false);
        flagScrollView.SetActive(true);
    }

    public void ItemCloseButton()
    {



        coverPanel.SetActive(false);
        itemPanel.SetActive(false);
        if(itemScrollView.activeSelf == true)
        {
            itemScrollView.SetActive(false);
        }
        else if(clothesScrollView.activeSelf == true)
        {
            clothesScrollView.SetActive(false);
        }
        else if(toolScrollView.activeSelf == true)
        {
            toolScrollView.SetActive(false);
        }
        else if(flagScrollView.activeSelf == true)
        {
            flagScrollView.SetActive(false);
        }
    }    

    public void CoinButton()
    {
        coverPanel.SetActive(true);
        coinPanel.SetActive(true);
    }

    public void CoinCloseButton()
    {
        coverPanel.SetActive(false);
        coinPanel.SetActive(false);
    }

  
    
}
