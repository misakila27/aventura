﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;


public class ResourceLoad : MonoBehaviour
{
    #region variable
    public Transform characterRoot;
    public CharacterBuffer characterBuffer;
    public Image characterImage;


    public CharacterList character;

    public GameObject CharacterSelectButton1;
    public GameObject CharacterSelectButton2;
    public GameObject CharacterSelectButton3;
    public GameObject CharacterSelectButton4;
    public GameObject CharacterSelectButton5;
    public GameObject CharacterSelectButton6;
    public GameObject CharacterSelectButton7;
    public GameObject CharacterSelectButton8;
    public GameObject CharacterSelectButton9;

    public Sprite CharacterSelectButton1Image;
    public Sprite CharacterSelectButton2Image;
    public Sprite CharacterSelectButton3Image;
    public Sprite CharacterSelectButton4Image;
    public Sprite CharacterSelectButton5Image;
    public Sprite CharacterSelectButton6Image;
    public Sprite CharacterSelectButton7Image;
    public Sprite CharacterSelectButton8Image;
    public Sprite CharacterSelectButton9Image;
    public Sprite CharacterSelectButton10Image;

    public Sprite[] flagImage = new Sprite[6];


    public Sprite[] toolImage = new Sprite[6];
 

    public Sprite[] clothesImage = new Sprite[6];

    public Sprite[] itemImage = new Sprite[18];

    public int characterChoice;
    public Image CharacterButton1;
    public Image CharacterButton2;
    public Image CharacterButton3;
    public Image itemButton;
    public Image clothesButton;
    public Image toolButton;
    public Image flagButton;

    public GameObject cos_flag3;
    public GameObject cos_tool1;

    GameObject cos_flag;
    GameObject cos_tool;

    public int choiceCharacterButton;

    public Button[] characterSelect = new Button[9];

    public int choiceCharacter1;
    public int choiceCharacter2;
    public int choiceCharacter3;

 


    private bool[] characterSlot1 = new bool[11];

    private bool[] characterSlot2 = new bool[11];

    private bool[] characterSlot3 = new bool[11];

    public int slot1Character;
    public int slot2Character;
    public int slot3Character;

    public Text firstCharacterName;
    public Text secondCharacterName;
    public Text thirdCharacterName;
    public Text itemName;
    public Text itemDescription;

    public GameObject Obj_ResourceLoad;

    public Text[] itemNumText = new Text[20];
    public int[] itemCount = new int[20];
    public int coinCount;
    public Text coin;

    private int itemPrice;

    #endregion


    private List<CharacterData> characters;

    private static ResourceLoad _instance = null;

    public static ResourceLoad Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new ResourceLoad();

            }
            return _instance;
        }
    }

    void Start()
    {
        DontDestroyOnLoad(Obj_ResourceLoad);
        firstCharacterName.text = "";
        secondCharacterName.text = "";
        thirdCharacterName.text = "";
        itemName.text = "";
        itemDescription.text = "";

        coinCount = 10;

        coin.text = coinCount.ToString();

        characterChoice = 0;
        toolImage[0] = itemButton.sprite;

        slot1Character = 0;
        slot2Character = 0;
        slot3Character = 0;

        for (int i = 0; i < 10; i++)
        {
            characterSlot1[i] = true;
            characterSlot2[i] = true;
            characterSlot3[i] = true;
        }

        choiceCharacter1 = 0;
        choiceCharacter2 = 0;
        choiceCharacter3 = 0;

        characters = new List<CharacterData>();

        int characterCount = 9;

        for (int i = 0; i < characterCount; i++)
        {
            var character = characterRoot.GetChild(i).GetComponent<CharacterData>();

            if (i < characterBuffer.character.Count)
            {
                character.SetCharacter(characterBuffer.character[i]);
            }
            characters.Add(character);

        }
    }

    public void ChoiceCharacterButton(int a)
    {
        characterImage.sprite = null;
        switch (a)
        {
            case 0:
                break;
            case 1:
                toolButton.sprite = toolImage[0];
                flagButton.sprite = flagImage[0];
                clothesButton.sprite = clothesImage[0];
                choiceCharacterButton = 1;
                if (choiceCharacter1 != 0)
                {
                  for(int i = 0; i< 9; i++)
                    {
                        characterSlot1[i] = true ;
                    }
                }
                break;
            case 2:
                toolButton.sprite = toolImage[0];
                flagButton.sprite = flagImage[0];
                clothesButton.sprite = clothesImage[0];
                choiceCharacterButton = 2;
                if (choiceCharacter2 != 0)
                {
                    for (int i = 0; i < 9; i++)
                    {
                        characterSlot2[i] = true;
                    }
                }
                break;
            case 3:
                toolButton.sprite = toolImage[0];
                flagButton.sprite = flagImage[0];
                clothesButton.sprite = clothesImage[0];
                choiceCharacterButton = 3;
                if (choiceCharacter3 != 0)
                {
                    for (int i = 0; i < 9; i++)
                    {
                        characterSlot3[i] = true;
                    }
                }
                break;
            default:
                break;
        }
       
    }

    void C_1()
    {
        for(int i = 0; i<10; i++)
        {
            characterSlot1[i] = true;
        }
    }

    void C_2()
    {
        for (int i = 0; i < 10; i++)
        {
            characterSlot2[i] = true;
        }
    }

    void C_3()
    {
        for (int i = 0; i < 10; i++)
        {
            characterSlot3[i] = true;
        }
    }

    void CharacterSlotAcive()
    {
     for(int i = 1; i< 10; i++)
        {
            if (characterSlot1[i] == false || characterSlot2[i] == false || characterSlot3[i] == false)
            {
                characterSelect[i].interactable = false;
            }
            else
                characterSelect[i].interactable = true;
        }
    }

    public void UpdateCoin()
    {
        coin.text = coinCount.ToString();
    }


    void ChoiceCharacterImage(int button)
    {

    }

    public void BuyItemPrice(int value)
    {
        switch (value)
        {
            case 1:
                itemPrice = 3;
                break;
            case 2:
                itemPrice = 5;
                break;
            case 3:
                itemPrice = 7;
                break;
            case 4:
                itemPrice = 3;
                break;
            case 5:
                itemPrice = 5;
                break;
            case 6:
                itemPrice = 7;
                break;
            case 7:
                itemPrice = 5;
                break;
            case 8:
                itemPrice = 7;
                break;
            case 9:
                itemPrice = 7;
                break;
            case 10:
                itemPrice = 7;
                break;
            case 11:
                itemPrice = 7;
                break;
            case 12:
                itemPrice = 10;
                break;
            case 13:
                itemPrice = 10;
                break;
            case 14:
                itemPrice = 10;
                break;
            case 15:
                itemPrice = 10;
                break;
            case 16:
                itemPrice = 10;
                break;
            case 17:
                itemPrice = 10;
                break;




        }


    }

    public void BuyItem(int itemnum)
    {
        if (coinCount - itemPrice >= 0)
        {
            switch (itemnum)
            {
                case 1:
                    itemCount[0]++;
                    itemNumText[0].text = itemCount[0].ToString();
                    coinCount = coinCount - itemPrice;
                    coin.text = coinCount.ToString();
                    UpdateCoin();
                    break;
                case 2:
                    itemCount[1]++;
                    itemNumText[1].text = itemCount[1].ToString();
                    coinCount = coinCount - itemPrice;
                    coin.text = coinCount.ToString();
                    UpdateCoin();
                    break;
                case 3:
                    itemCount[2]++;
                    itemNumText[2].text = itemCount[2].ToString();
                    coinCount = coinCount - itemPrice;
                    coin.text = coinCount.ToString();
                    UpdateCoin();
                    break;
                case 4:
                    itemCount[3]++;
                    itemNumText[3].text = itemCount[3].ToString();
                    coinCount = coinCount - itemPrice;
                    coin.text = coinCount.ToString();
                    UpdateCoin();
                    break;
                case 5:
                    itemCount[4]++;
                    itemNumText[4].text = itemCount[4].ToString();
                    coinCount = coinCount - itemPrice;
                    coin.text = coinCount.ToString();
                    UpdateCoin();
                    break;
                case 6:
                    itemCount[5]++;
                    itemNumText[5].text = itemCount[5].ToString();
                    coinCount = coinCount - itemPrice;
                    coin.text = coinCount.ToString();
                    UpdateCoin();
                    break;
                case 7:
                    itemCount[6]++;
                    itemNumText[6].text = itemCount[6].ToString();
                    coinCount = coinCount - itemPrice;
                    coin.text = coinCount.ToString();
                    UpdateCoin();
                    break;
                case 8:
                    itemCount[7]++;
                    itemNumText[7].text = itemCount[7].ToString();
                    coinCount = coinCount - itemPrice;
                    coin.text = coinCount.ToString();
                    UpdateCoin();
                    break;
                case 9:
                    itemCount[8]++;
                    itemNumText[8].text = itemCount[8].ToString();
                    coinCount = coinCount - itemPrice;
                    coin.text = coinCount.ToString();
                    UpdateCoin();
                    break;
                case 10:
                    itemCount[9]++;
                    itemNumText[9].text = itemCount[9].ToString();
                    coinCount = coinCount - itemPrice;
                    coin.text = coinCount.ToString();
                    UpdateCoin();
                    break;
                case 11:
                    itemCount[10]++;
                    itemNumText[10].text = itemCount[10].ToString();
                    coinCount = coinCount - itemPrice;
                    coin.text = coinCount.ToString();
                    UpdateCoin();
                    break;
                case 12:
                    itemCount[11]++;
                    itemNumText[11].text = itemCount[11].ToString();
                    coinCount = coinCount - itemPrice;
                    coin.text = coinCount.ToString();
                    UpdateCoin();
                    break;
                case 13:
                    itemCount[12]++;
                    itemNumText[12].text = itemCount[12].ToString();
                    coinCount = coinCount - itemPrice;
                    coin.text = coinCount.ToString();
                    UpdateCoin();
                    break;
                case 14:
                    itemCount[13]++;
                    itemNumText[13].text = itemCount[13].ToString();
                    coinCount = coinCount - itemPrice;
                    coin.text = coinCount.ToString();
                    UpdateCoin();
                    break;
                case 15:
                    itemCount[14]++;
                    itemNumText[14].text = itemCount[14].ToString();
                    coinCount = coinCount - itemPrice;
                    coin.text = coinCount.ToString();
                    UpdateCoin();
                    break;
                case 16:
                    itemCount[15]++;
                    itemNumText[15].text = itemCount[15].ToString();
                    coinCount = coinCount - itemPrice;
                    coin.text = coinCount.ToString();
                    UpdateCoin();
                    break;
                case 17:
                    itemCount[16]++;
                    itemNumText[16].text = itemCount[16].ToString();
                    coinCount = coinCount - itemPrice;
                    coin.text = coinCount.ToString();
                    UpdateCoin();
                    break;


            }

        }

    }

    public void PickItem(int itemnum)
    {
          switch (itemnum)
            {
                case 1:
                    if(itemCount[0] > 0)
                    {
                        itemCount[0]--;
                        itemNumText[0].text = itemCount[0].ToString();
                    }
                    break;
                case 2:
                if (itemCount[1] > 0)
                {
                    itemCount[1]--;
                    itemNumText[1].text = itemCount[1].ToString();
                }
                break;
                case 3:
                if (itemCount[1] > 0)
                {
                    itemCount[2]--;
                    itemNumText[2].text = itemCount[2].ToString();
                }
                break;
                case 4:
                if (itemCount[3] > 0)
                {
                    itemCount[3]--;
                    itemNumText[3].text = itemCount[3].ToString();
                }
                break;
                case 5:
                if (itemCount[4] > 0)
                {
                    itemCount[4]--;
                    itemNumText[4].text = itemCount[4].ToString();
                }
                break;
                case 6:
                if (itemCount[5] > 0)
                {
                    itemCount[5]--;
                    itemNumText[5].text = itemCount[5].ToString();
                }
                break;
                case 7:
                if (itemCount[6] > 0)
                {
                    itemCount[6]--;
                    itemNumText[6].text = itemCount[6].ToString();
                }
                break;
                case 8:
                if (itemCount[7] > 0)
                {
                    itemCount[7]--;
                    itemNumText[7].text = itemCount[7].ToString();
                }
                break;
                case 9:
                if (itemCount[8] > 0)
                {
                    itemCount[8]--;
                    itemNumText[8].text = itemCount[8].ToString();
                }
                break;
                case 10:
                if (itemCount[9] > 0)
                {
                    itemCount[9]--;
                    itemNumText[9].text = itemCount[9].ToString();
                }
                break;
                case 11:
                if (itemCount[10] > 0)
                {
                    itemCount[10]--;
                    itemNumText[10].text = itemCount[10].ToString();
                }
                break;
                case 12:
                if (itemCount[11] > 0)
                {
                    itemCount[11]--;
                    itemNumText[11].text = itemCount[11].ToString();
                }
                break;
                case 13:
                if (itemCount[12] > 0)
                {
                    itemCount[12]--;
                    itemNumText[12].text = itemCount[12].ToString();
                }
                break;
                case 14:
                if (itemCount[13] > 0)
                {
                    itemCount[13]--;
                    itemNumText[13].text = itemCount[13].ToString();
                }
                break;
                case 15:
                if (itemCount[14] > 0)
                {
                    itemCount[14]--;
                    itemNumText[14].text = itemCount[14].ToString();
                }
                break;
                case 16:
                if (itemCount[15] > 0)
                {
                    itemCount[15]--;
                    itemNumText[15].text = itemCount[15].ToString();
                }
                break;
                case 17:
                if (itemCount[16] > 0)
                {
                    itemCount[16]--;
                    itemNumText[16].text = itemCount[16].ToString();
                }
                break;


            }

        

    }


    public void CharcterImage(int characterSelect)
    {
        characterChoice = characterSelect;
        Debug.Log("asd");

        switch (characterChoice)
        {
                
            case 1: //아드먼
                characterImage.sprite = CharacterSelectButton1Image;
                switch (choiceCharacterButton)
                {
                    case 1:
                        CharacterButton1.sprite = characterImage.sprite;
                        C_1();
                        characterSlot1[1] = false;
                        CharacterSlotAcive();
                        slot1Character = 1;
                        firstCharacterName.text = "아드먼";
                        break;
                    case 2:
                        CharacterButton2.sprite = characterImage.sprite;
                        C_2();
                        characterSlot2[1] = false;
                        CharacterSlotAcive();
                        slot2Character = 1;
                        secondCharacterName.text = "아드먼";
                        break;
                    case 3:
                        CharacterButton3.sprite = characterImage.sprite;
                        C_3();
                        characterSlot3[1] = false;
                        CharacterSlotAcive();
                        slot3Character = 1;
                        thirdCharacterName.text = "아드먼";
                        break;
                    default:
                        break;
                }

                break;
            case 2: //리수
                characterImage.sprite = CharacterSelectButton2Image;
                switch (choiceCharacterButton)
                {
                    case 1:
                        CharacterButton1.sprite = characterImage.sprite;
                        C_1();
                        characterSlot1[2] = false;
                        CharacterSlotAcive();
                            slot1Character = 2;
                        firstCharacterName.text = "리수";
                        break;
                    case 2:
                        CharacterButton2.sprite = characterImage.sprite;
                        C_2();
                        characterSlot2[2] = false;
                        CharacterSlotAcive();
                        slot2Character = 2;
                        secondCharacterName.text = "리수";
                        break;
                    case 3:
                        CharacterButton3.sprite = characterImage.sprite;
                        C_3();
                        characterSlot3[2] = false;
                        CharacterSlotAcive();
                        slot3Character = 2;
                        thirdCharacterName.text = "리수";
                        break;
                    default:
                        break;
                }
                break;
            case 3: // 보리스

                characterImage.sprite = CharacterSelectButton3Image;
                switch (choiceCharacterButton)
                {
                    case 1:
                        CharacterButton1.sprite = characterImage.sprite;
                        C_1();
                        characterSlot1[3] = false;
                        CharacterSlotAcive();
                        slot1Character = 3;
                        firstCharacterName.text = "보리스";
                        break;
                    case 2:
                        CharacterButton2.sprite = characterImage.sprite;
                        C_2();
                        characterSlot2[3] = false;
                        CharacterSlotAcive();
                        slot2Character = 3;
                        secondCharacterName.text = "보리스";
                        break;
                    case 3:
                        CharacterButton3.sprite = characterImage.sprite;
                        C_3();
                        characterSlot3[3] = false;
                        CharacterSlotAcive();
                        slot3Character = 3;
                        thirdCharacterName.text = "보리스";
                        break;
                    default:
                        break;
                }
                break;
            case 4: //세드
                characterImage.sprite = CharacterSelectButton4Image;
               
                switch (choiceCharacterButton)
                {
                    case 1:
                        CharacterButton1.sprite = characterImage.sprite;
                        C_1();
                        characterSlot1[4] = false;
                        CharacterSlotAcive();
                        slot1Character = 4;
                        firstCharacterName.text = "세드";
                        break;
                    case 2:
                        CharacterButton2.sprite = characterImage.sprite;
                        C_2();
                        characterSlot2[4] = false;
                        CharacterSlotAcive();
                        slot2Character = 4;
                        secondCharacterName.text = "세드";
                        break;
                    case 3:
                        CharacterButton3.sprite = characterImage.sprite;
                        C_3();
                        characterSlot3[4] = false;
                        CharacterSlotAcive();
                        slot3Character = 4;
                        thirdCharacterName.text = "세드";
                        break;
                    default:
                        break;
                }
                break;
            case 5: //스텀
                characterImage.sprite = CharacterSelectButton5Image;
                
                switch (choiceCharacterButton)
                {
                    case 1:
                        CharacterButton1.sprite = characterImage.sprite;
                        C_1();
                        characterSlot1[5] = false;
                        CharacterSlotAcive();
                        slot1Character = 5;
                        firstCharacterName.text = "스텀";
                        break;
                    case 2:
                        CharacterButton2.sprite = characterImage.sprite;
                        C_2();
                        characterSlot2[5] = false;
                        CharacterSlotAcive();
                        slot2Character = 5;
                        secondCharacterName.text = "스텀";
                        break;
                    case 3:
                        CharacterButton3.sprite = characterImage.sprite;
                        C_3();
                        characterSlot3[5] = false;
                        CharacterSlotAcive();
                        slot3Character = 5;
                        thirdCharacterName.text = "스텀";
                        break;
                    default:
                        break;
                }
                break;
            case 6: //오필리아
                characterImage.sprite = CharacterSelectButton6Image;
                
                switch (choiceCharacterButton)
                {
                    case 1:
                        CharacterButton1.sprite = characterImage.sprite;
                        C_1();
                        characterSlot1[6] = false;
                        CharacterSlotAcive();
                        slot1Character = 6;
                        firstCharacterName.text = "오필리아";
                        break;
                    case 2:
                        CharacterButton2.sprite = characterImage.sprite;
                        C_2();
                        characterSlot2[6] = false;
                        CharacterSlotAcive();
                        slot2Character = 6;
                        secondCharacterName.text = "오필리아";
                        break;
                    case 3:
                        CharacterButton3.sprite = characterImage.sprite;
                        C_3();
                        characterSlot3[6] = false;
                        CharacterSlotAcive();
                        slot3Character = 6;
                        thirdCharacterName.text = "오필리아";
                        break;
                    default:
                        break;
                }
                break;
            case 7: //자니
                characterImage.sprite = CharacterSelectButton7Image;

                switch (choiceCharacterButton)
                {
                    case 1:
                        CharacterButton1.sprite = characterImage.sprite;
                        C_1();
                        characterSlot1[7] = false;
                        CharacterSlotAcive();
                        slot1Character = 7;
                        firstCharacterName.text = "자니";
                        break;
                    case 2:
                        CharacterButton2.sprite = characterImage.sprite;
                        C_2();
                        characterSlot2[7] = false;
                        CharacterSlotAcive();
                        slot2Character = 7;
                       secondCharacterName.text = "자니";
                        break;
                    case 3:
                        CharacterButton3.sprite = characterImage.sprite;
                        C_3();
                        characterSlot3[7] = false;
                        CharacterSlotAcive();
                        slot3Character = 7;
                        thirdCharacterName.text = "자니";
                        break;
                    default:
                        break;
                }
                break;
            case 8: //주디
                characterImage.sprite = CharacterSelectButton8Image;

                switch (choiceCharacterButton)
                {
                    case 1:
                        CharacterButton1.sprite = characterImage.sprite;
                        C_1();
                        characterSlot1[8] = false;
                        CharacterSlotAcive();
                        slot1Character = 8;
                        firstCharacterName.text = "주디";
                        break;
                    case 2:
                        CharacterButton2.sprite = characterImage.sprite;
                        C_2();
                        characterSlot2[8] = false;
                        CharacterSlotAcive();
                        slot2Character = 8;
                       secondCharacterName.text = "주디";
                        break;
                    case 3:
                        CharacterButton3.sprite = characterImage.sprite;
                        C_3();
                        characterSlot3[8] = false;
                        CharacterSlotAcive();
                        slot3Character = 8;
                        thirdCharacterName.text = "주디";
                        break;
                    default:
                        break;
                }
                break;
            case 9: //트롯
                characterImage.sprite = CharacterSelectButton9Image;

                switch (choiceCharacterButton)
                {
                    case 1:
                        CharacterButton1.sprite = characterImage.sprite;
                        C_1();
                        characterSlot1[9] = false;
                        CharacterSlotAcive();
                        slot1Character = 9;
                        firstCharacterName.text = "트롯";
                        break;
                    case 2:
                        CharacterButton2.sprite = characterImage.sprite;
                        C_2();
                        characterSlot2[9] = false;
                        CharacterSlotAcive();
                        slot2Character = 9;
                        secondCharacterName.text = "트롯";
                        break;
                    case 3:
                        CharacterButton3.sprite = characterImage.sprite;
                        C_3();
                        characterSlot3[9] = false;
                        CharacterSlotAcive();
                        slot3Character = 9;
                        thirdCharacterName.text = "트롯";
                        break;
                    default:
                        break;
                }
                break;
            default:
                characterImage.sprite = null;
                break;

        }
 
       
    }
   
    public void ItemImage(int itemChoice)
    {
        switch (itemChoice)
        {

            case 1:
                itemButton.sprite = itemImage[1];
                itemName.text = "건빵";
                itemDescription.text = "식량 +30";
                break;
            case 2:
                itemButton.sprite = itemImage[2];
                itemName.text = "육포";
                itemDescription.text = "식량 +50";
                break;
            case 3:
                itemButton.sprite = itemImage[3];
                itemName.text = "전투 식량";
                itemDescription.text = "식량 +80";
                break;
            case 4:
                itemButton.sprite = itemImage[4];
                itemName.text = "약초";
                itemDescription.text = "체력 +30";
                break;
            case 5:
                itemButton.sprite = itemImage[5];
                itemName.text = "삼가루";
                itemDescription.text = "체력 +50";
                break;
            case 6:
                itemButton.sprite = itemImage[6];
                itemName.text = "방향제";
                itemDescription.text = "정신력 +30";
                break;
            case 7:
                itemButton.sprite = itemImage[7];
                itemName.text = "성수";
                itemDescription.text = "정신력 +50";
                break;
            case 8:
                itemButton.sprite = itemImage[8];
                itemName.text = "각성제";
                itemDescription.text = "정신력 +80";
                break;
            case 9:
                itemButton.sprite = itemImage[9];
                itemName.text = "장어구이";
                itemDescription.text = "식량 +30, 체력 +30";
                break;
            case 10:
                itemButton.sprite = itemImage[10];
                itemName.text = "솜사탕";
                itemDescription.text = "체력 +30 " +
                    "정신력 +30";
                break;
            case 11:
                itemButton.sprite = itemImage[11];
                itemName.text = "커피 믹스";
                itemDescription.text = "식량 +30, 정신력 +30";
                break;
            case 12:
                itemButton.sprite = itemImage[12];
                itemName.text = "엄마 도시락";
                itemDescription.text = "식량 +30, 체력 +30, 정신력 +30";
                break;
            case 13:
                itemButton.sprite = itemImage[13];
                itemName.text = "럭키 박스";
                itemDescription.text = "??????";
                break;
            case 14:
                itemButton.sprite = itemImage[14];
                itemName.text = "망원경";
                itemDescription.text = "사용 즉시 일정 반경의 구름이 걷힘";
                break;
            case 15:
                itemButton.sprite = itemImage[15];
                itemName.text = "탐지봉";
                itemDescription.text = "일정 반경 내 거점 위치가 지도에 표시";
                break;
            case 16:
                itemButton.sprite = itemImage[16];
                itemName.text = "마법 팬티";
                itemDescription.text = "사용한 날 이벤트가 생존요소에 영향을 끼치지 않음";
                break;
            case 17:
                itemButton.sprite = itemImage[17];
                itemName.text = "러닝 슈즈";
                itemDescription.text = "이동거리 10% 증가";
                break;

            default:
                itemButton.sprite = toolImage[0];
                break;
                

        }
    }
  
    public void ClothesChoice(int clothesChoice)
    {
        switch (clothesChoice)
        {

            case 1:
                characterImage.sprite = CharacterSelectButton1Image;
                clothesButton.sprite = clothesImage[1];
                break;
            case 2:
                characterImage.sprite = CharacterSelectButton10Image;
                clothesButton.sprite = clothesImage[2];
                break;
            case 3:
                characterImage.sprite = CharacterSelectButton1Image;
                clothesButton.sprite = clothesImage[3];
                break;
            case 4:
                clothesButton.sprite = clothesImage[4];
                break;
            case 5:
                clothesButton.sprite = clothesImage[5];
                break;
            default:
                clothesButton.sprite = clothesImage[0];
                break;


        }
    }

    public void FlagChoice(int flagChoice)
    {
        switch (flagChoice)
        {

            case 1:
                Destroy(cos_flag);
                flagButton.sprite = flagImage[1];
                break;
            case 2:
                Destroy(cos_flag);
                flagButton.sprite = flagImage[2];
                break;
            case 3:
                cos_flag = Instantiate(cos_flag3,new Vector3(-2.33f, 2.73f, 0), Quaternion.identity);
                cos_flag.name = "cos_flag";
                flagButton.sprite = flagImage[3];
                break;
            case 4:
                flagButton.sprite = flagImage[4];
                break;
            case 5:
                flagButton.sprite = flagImage[5];
                break;
            default:
                flagButton.sprite = flagImage[0];
                break;


        }

    }

    public void ToolChoice(int toolChoice)
    {
        switch (toolChoice)
        {

            case 1:
                cos_tool = Instantiate(cos_tool1, new Vector3(-1.642f, 2.259f, 0), Quaternion.identity,GameObject.FindGameObjectWithTag("CharacterPanel").transform);
                cos_tool.name = "cos_tool";
                toolButton.sprite = toolImage[1];
                break;
            case 2:
                Destroy(cos_tool);
                toolButton.sprite = toolImage[2];
                break;
            case 3:
                Destroy(cos_tool);
                toolButton.sprite = toolImage[3];
                break;
            case 4:
                toolButton.sprite = toolImage[4];
                break;
            case 5:
                toolButton.sprite = toolImage[5];
                break;
            default:
                toolButton.sprite = toolImage[0];
                break;

        }
    }

        void Update()
    {
      
    }
}
