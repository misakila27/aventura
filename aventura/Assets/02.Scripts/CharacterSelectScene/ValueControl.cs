﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ValueControl : MonoBehaviour
{
   public int supplyPoint;
    public int healthPoint;
    public int mentalPoint;
    public int levelPoint;



    public Text health;
    public Text supply;
    public Text mental;
    public Text level;


    public void HealthPlus()
    {
        if(levelPoint>0)
        {
            healthPoint += 1;
            levelPoint -= 1;
            UpdateLevelText();
            health.text = healthPoint.ToString();
        }
      
    }

    public void HealthMinus()
    {
        if(healthPoint > 0)
        {
            healthPoint -= 1;
            levelPoint += 1;
            UpdateLevelText();
        }
        health.text = healthPoint.ToString();
    }

    public void SupplyPlus()
    {
        if (levelPoint > 0)
        {
            supplyPoint += 1;
            levelPoint -= 1;
            UpdateLevelText();
            supply.text = supplyPoint.ToString();
        }
    }

    public void SupplyMinus()
    {
        if(supplyPoint > 0)
        {
            supplyPoint -= 1;
            levelPoint += 1;
            UpdateLevelText();
        }
        supply.text = supplyPoint.ToString();
    }
    public void MentalPlus()
    {
        if (levelPoint > 0)
        {
            mentalPoint += 1;
            levelPoint -= 1;
            UpdateLevelText();
            mental.text = mentalPoint.ToString();
        }
    }

    public void MentalMinus()
    {
        if(mentalPoint > 0)
        {
            mentalPoint -= 1;
            levelPoint += 1;
            UpdateLevelText();
        }
        mental.text = mentalPoint.ToString();
    }

    public void UpdateLevelText()
    {
        level.text = levelPoint.ToString();
    }




    // Start is called before the first frame update
    void Start()
    {
        supplyPoint = 0;
        healthPoint = 0;
        mentalPoint = 0;
        levelPoint = 10; //추후 수정
        level.text = levelPoint.ToString();


      
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
