﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class DialogueManager : MonoBehaviour
{

    private int choiceDialogue;
   
    public SpriteRenderer[] choice = new SpriteRenderer[3];
    public Text[] choiceDialougeText = new Text[4];
    public bool waitNextDay;
    
    public InGameUiControl uiControl;
    public Button[] dialougeButton = new Button[3];

    public bool[] dialogueCheck = new bool[20];

    [SerializeField]
    private int dialouge;

    // Start is called before the first frame update
    void Start() 
    {
       
        // dialouge = 101000; //지역/비어둠/답변그룹/답변/선택지그룹/선택지
        waitNextDay = true;
        NextDialouge();
      
     
        choiceDialougeText[0].gameObject.SetActive(true);
        choiceDialougeText[1].gameObject.SetActive(false);
        choiceDialougeText[2].gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public int StartDialouge()
    {
         int value = Random.Range(1, 12);
        // int value = 1;
    


        return value;
    }

    int ReturnPercent()
    {
       int value = Random.Range(0, 100);
  

       return value;


    }


   public void ChoiceDIalouge1()
    {
        choiceDialogue = 1;
        ChoiceDialogue();
    }

   public void ChoiceDIalouge2()
    {
        choiceDialogue = 2;
        ChoiceDialogue();
    }
   public void ChoiceDIalouge3()
    {
        choiceDialogue = 3;
        ChoiceDialogue();
    }

    public void NextDialouge()
    {
        ChoiceDIalouge1();
        if (waitNextDay == true)
        {
            dialougeButton[0].interactable = true;
            dialougeButton[1].interactable = true;
            dialougeButton[2].interactable = true;
            choice[0].gameObject.SetActive(true);
            choice[1].gameObject.SetActive(true);
            choice[2].gameObject.SetActive(true);
            waitNextDay = false;
            switch (StartDialouge())
            {

                case 1:
                    if (dialogueCheck[1] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 101000;
                        SelectDialgoue();
                        Dialouge();
                        dialogueCheck[1] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                        NextDialouge();
                    break;
              
                case 2:
                    if (dialogueCheck[2] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 102000;
                        SelectDialgoue();
                        Dialouge();
                        dialogueCheck[2] = true;
                        break;
                    }
                    else
                          waitNextDay = true;
                        NextDialouge();
                    break;
                case 3:
                    if (dialogueCheck[3] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 103000;
                        SelectDialgoue();
                        Dialouge();
                        dialogueCheck[3] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                    NextDialouge();
                    break;
                case 4: //식량에서 이상한 냄새 
                    if (dialogueCheck[4] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 104000;
                        Dialouge();
                        SelectDialgoue();
                 
                        dialogueCheck[4] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                    NextDialouge();
                    break;

                case 5: //벌레가 식량을 
                    if (dialogueCheck[5] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 105000;
                        Dialouge();
                        SelectDialgoue();
                        dialogueCheck[5] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                    NextDialouge();
                    break;
                case 6: // 더러운 습지가 
                    if (dialogueCheck[6] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 106000;
                        SelectDialgoue();
                        Dialouge();
                        dialogueCheck[6] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                    NextDialouge();
                    break;
                case 7:
                    if (dialogueCheck[7] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 107000;
                        SelectDialgoue();
                        Dialouge();
                        dialogueCheck[7] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                    NextDialouge();
                    break;
                case 8:
                    if (dialogueCheck[8] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 108000;
                        SelectDialgoue();
                        Dialouge();
                        dialogueCheck[8] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                    NextDialouge();
                    break;
                case 9:
                    if (dialogueCheck[9] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 109000;
                        SelectDialgoue();
                        Dialouge();
                        dialogueCheck[9] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                    NextDialouge();
                    break;
                case 10:
                    if (dialogueCheck[10] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 110000;
                        SelectDialgoue();
                        Dialouge();
                        dialogueCheck[10] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                    NextDialouge();
                    break;
                case 11:
                    if (dialogueCheck[11] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 111000;
                        SelectDialgoue();
                        Dialouge();
                        dialogueCheck[11] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                    NextDialouge();
                    break;
                case 12:
                    if (dialogueCheck[12] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 112000;
                        SelectDialgoue();
                        Dialouge();
                        dialogueCheck[12] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                    NextDialouge();
                    break;
                case 13:
                    if (dialogueCheck[13] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 113000;
                        SelectDialgoue();
                        Dialouge();
                        dialogueCheck[13] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                    NextDialouge();
                    break;
                case 14:
                    if (dialogueCheck[14] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 114000;
                        SelectDialgoue();
                        Dialouge();
                        dialogueCheck[14] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                    NextDialouge();
                    break;
                case 15:
                    if (dialogueCheck[15] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 115000;
                        SelectDialgoue();
                        Dialouge();
                        dialogueCheck[15] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                    NextDialouge();
                    break;
                case 16:
                    if (dialogueCheck[16] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 116000;
                        SelectDialgoue();
                        Dialouge();
                        dialogueCheck[16] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                    NextDialouge();
                    break;
                case 17:
                    if (dialogueCheck[17] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 117000;
                        SelectDialgoue();
                        Dialouge();
                        dialogueCheck[17] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                    NextDialouge();
                    break;

                case 18:
                    if (dialogueCheck[18] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 118000;
                        SelectDialgoue();
                        Dialouge();
                        dialogueCheck[18] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                    NextDialouge();
                    break;

                case 19:
                    if (dialogueCheck[19] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 119000;
                        SelectDialgoue();
                        Dialouge();
                        dialogueCheck[19] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                    NextDialouge();
                    break;

                case 20:
                    if (dialogueCheck[20] == false)
                    {
                        uiControl.PlusDay();
                        dialouge = 120000;
                        SelectDialgoue();
                        Dialouge();
                        dialogueCheck[20] = true;
                        break;
                    }
                    else
                        waitNextDay = true;
                    NextDialouge();
                    break;

            }
        }
        else 
        {
            Dialouge();
        }
       

    }
    

    void ChoiceDialogue()
    {
        switch (choiceDialogue)
        {
            case 1:
                choice[0].sortingOrder = 4;
                choice[1].sortingOrder = 3;
                choice[2].sortingOrder = 2;

                choiceDialougeText[0].gameObject.SetActive(true);
                choiceDialougeText[1].gameObject.SetActive(false);
                choiceDialougeText[2].gameObject.SetActive(false);

                break;
            case 2:
                choice[0].sortingOrder = 3;
                choice[1].sortingOrder = 4;
                choice[2].sortingOrder = 2;
                choiceDialougeText[0].gameObject.SetActive(false);
                choiceDialougeText[1].gameObject.SetActive(true);
                choiceDialougeText[2].gameObject.SetActive(false);
                break;
            case 3:
                choice[0].sortingOrder = 2;
                choice[1].sortingOrder = 3;
                choice[2].sortingOrder = 4;
                choiceDialougeText[0].gameObject.SetActive(false);
                choiceDialougeText[1].gameObject.SetActive(false);
                choiceDialougeText[2].gameObject.SetActive(true);
                break;

            default:
                choice[0].sortingOrder = 3;
                choice[1].sortingOrder = 2;
                choice[2].sortingOrder = 1;
                choiceDialougeText[0].gameObject.SetActive(true);
                choiceDialougeText[1].gameObject.SetActive(false);
                choiceDialougeText[2].gameObject.SetActive(false);
                break;


        }
        
    }

    public void Dialouge() // 답변 위치 
    {
        switch (dialouge)
        {
            case 101000:
                
                choiceDialougeText[3].text = "보물 상자다! 근데 왜 앞에 해골이 있지?";
                break;
            case 101001:
                int Percent = ReturnPercent();
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[0].text = "";

                if (Percent <= 70)
                {
                    choiceDialougeText[3].text = "미믹이다!";
                    uiControl.MinusHealth(4.0f);
                    uiControl.MinusSupply(4.0f);
                    uiControl.MinusMental(4.0f);
                    waitNextDay = true;
                    
                }
                else if (Percent > 70)
                {
                    choiceDialougeText[3].text = "와우 해적물자다!";
                    uiControl.PlusHealth(5.0f);
                    uiControl.PlusMental(5.0f);
                    uiControl.PlusSupply(5.0f);
                    waitNextDay = true;
                }
                break;
            case 101002:
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[1].text = "";
                waitNextDay = true;
                        
                break;
            case 102000:
                choiceDialougeText[3].text = "벽에 무언가 반짝거리는게 박혀있다.";
                
                break;
            case 102001:
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[0].text = "";
                Percent = ReturnPercent();
                if (Percent <= 70)
                {
                    choiceDialougeText[3].text = "젠장! 몬스터잖아!";
                    uiControl.MinusHealth(4.0f);
                    uiControl.MinusSupply(4.0f);
                    uiControl.MinusMental(4.0f);
                    waitNextDay = true;

                }
                else if (Percent > 70)
                {
                    choiceDialougeText[3].text = "보석이다! 최고야!";
                    uiControl.PlusHealth(4.0f);
                    uiControl.PlusMental(4.0f);
                    uiControl.PlusSupply(4.0f);
                    waitNextDay = true;
                }
             
                break;
   
            case 103000:
                choiceDialougeText[3].text = "어디선가 꽃향기가 불어온다 '달달한 향이 아주 좋은걸' ";
                break;
            case 103001:
                choiceDialougeText[3].text = "나무에 달콤한 향이나는 거대한 꽃이 달려있다.";      
                choice[2].gameObject.SetActive(false);
                dialouge = 103100;
                SelectDialgoue();
           
               
                break;
            case 103002:
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[1].text = "";
                choiceDialougeText[3].text = "모험을 계속 한다.";
                waitNextDay = true;
                break;
          
            case 103101:
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[0].text = "";
                choiceDialougeText[3].text = "젠장! 식인식물이었나!";
                waitNextDay = true;
                break;
            case 103102:
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[1].text = "";
                choiceDialougeText[3].text = "모험을 계속 한다.";
                waitNextDay = true;
                break;
            case 104000:
                choiceDialougeText[3].text = "식량에서 이상한 냄새가 나는것 같다.";
                break;
            case 104001:
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[0].text = "";

                choiceDialougeText[3].text = "윽....배가..아프다..";
                uiControl.MinusHealth(15.0f);
                waitNextDay = true;
                break;
            case 104002:
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[1].text = "";
                choiceDialougeText[3].text = "아깝지만 어쩔 수 없지..";
                uiControl.MinusSupply(10.0f);
                waitNextDay = true;
                break;
            case 105000:
                choiceDialougeText[3].text = "벌레가 식량을 파먹었다!";
                break;
            case 105001:
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[0].text = "";
                choiceDialougeText[3].text = "식감이 이상해!";
                uiControl.MinusMental(10.0f);
                waitNextDay = true;
                break;
            case 105002:
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[1].text = "";
                choiceDialougeText[3].text = "으 더러워";
                uiControl.MinusSupply(10.0f);
                waitNextDay = true;
                break;
            case 106000:
                choiceDialougeText[3].text = "더러운 습지가 길을 막고 있다.";
                break;
            case 106001:
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[0].text = "";
                choiceDialougeText[3].text = "냄새나고 질퍽거려..";
                uiControl.MinusMental(10.0f);
                waitNextDay = true;
                break;
            case 106002:
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[1].text = "";
                choiceDialougeText[3].text = "생각보다 멀리 돌아왔다.";
                uiControl.MinusHealth(10.0f);
                waitNextDay = true;
                break;
            case 107000:
                choiceDialougeText[3].text = "동물의 사체를 발견했다. 죽은지 얼마 안된 것 같다.";
                break;
            case 107001:
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[0].text = "";
                choiceDialougeText[3].text = "고기 최고!";
                uiControl.PlusSupply(10.0f);
                waitNextDay = true;
                break;
            case 108000:
                choiceDialougeText[3].text = "쉬기 좋은 장소를 발견했다.";
                break;
            case 108001:
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[0].text = "";
                choiceDialougeText[3].text = "오랜만에 잘 쉬었다.";
                uiControl.PlusHealth(10.0f);
                waitNextDay = true;
                break;
            case 109000:
                choiceDialougeText[3].text = "아름다운 자연 경관을 발견했다.";
                break;
            case 109001:
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[0].text = "";
                choiceDialougeText[3].text = "역시 자연은 위대해!";
                uiControl.PlusMental(10.0f);
                waitNextDay = true;
                break;
            case 110000:
                choiceDialougeText[3].text = "원숭이들이 나타났다. 우리에게 관심을 보이는 것 같다.";
                break;
            case 110001:
                Percent = ReturnPercent();
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[0].text = "";
           
                if (Percent <= 50)
                {
                    choiceDialougeText[3].text = "원숭이들이 과일을 선물했다.";
                    uiControl.PlusSupply(8.0f);
                    waitNextDay = true;

                }
                else if (Percent > 50)
                {
                    choiceDialougeText[3].text = "이놈들 식량을 훔쳐가잖아!";
                    uiControl.MinusSupply(10.0f);
                    waitNextDay = true;
                }
                 break;
            case 110002:
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[0].text = "";
                choiceDialougeText[1].text = "";

                choiceDialougeText[3].text = "이놈들 식량을 훔쳐가잖아!";
                uiControl.MinusSupply(8.0f);
                break;
            case 111000:
                choiceDialougeText[3].text = "으윽... 똥.. 똥이 마렵다";
                break;
            case 111001:
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[0].text = "";
                choiceDialougeText[3].text = "대원들이 혐오스럽게 쳐다본다";
                uiControl.MinusMental(15.0f);
                waitNextDay = true;
                break;
            case 111002:
                dialouge = 111100;
                SelectDialgoue();
                Dialouge();
                break;
            case 111100:
                choiceDialougeText[3].text = "어디로 가서 쌀까";
                break;
            case 111101:
                dialouge = 111200;
                SelectDialgoue();
                Dialouge();
                break;
            case 111102:
                dialouge = 111300;
                SelectDialgoue();
                Dialouge();
                break;

            case 111200:
                choiceDialougeText[3].text = "여기서 쌀까?";
                break;
            case 111201:
                Percent = ReturnPercent();
        
                choiceDialougeText[0].text = "";
                  if (Percent < 50)
                {
                    choice[0].gameObject.SetActive(false);
                    choice[1].gameObject.SetActive(false);
                    choice[2].gameObject.SetActive(false);
                    choiceDialougeText[0].text = "";
                    choiceDialougeText[1].text = "";
                    choiceDialougeText[3].text = "으아악! 바람이 불어 똥이 튄다.";
                    uiControl.MinusMental(15.0f);
                    waitNextDay = true;

                }
                else if (Percent > 50)
                {
                    choice[0].gameObject.SetActive(false);
                    choice[1].gameObject.SetActive(false);
                    choice[2].gameObject.SetActive(false);
                    choiceDialougeText[0].text = "";
                    choiceDialougeText[1].text = "";
                    choiceDialougeText[3].text = "시원하게 마무리했다!";
                    uiControl.PlusMental(5.0f);
                    waitNextDay = true;
                }
                break;
            case 111202:
                Percent = ReturnPercent();
           
                choiceDialougeText[0].text = "";

                if (Percent <= 50)
                {
                    choice[0].gameObject.SetActive(false);
                    choice[1].gameObject.SetActive(false);
                    choice[2].gameObject.SetActive(false);
                    choiceDialougeText[0].text = "";
                    choiceDialougeText[1].text = "";
                    choiceDialougeText[3].text = "아름다운 꽃밭이다. 여기로 정했다!";
                    uiControl.PlusHealth(8.0f);
                    uiControl.PlusMental(8.0f);
                    uiControl.PlusSupply(8.0f);
                    waitNextDay = true;

                }
                else if (Percent > 50 && Percent <= 80)
                {
                    dialouge = 111100;
                    SelectDialgoue();
                    Dialouge();
                }
                else if (Percent > 80)
                {
                    choice[0].gameObject.SetActive(false);
                    choice[1].gameObject.SetActive(false);
                    choice[2].gameObject.SetActive(false);
                    choiceDialougeText[0].text = "";
                    choiceDialougeText[1].text = "";
                    choiceDialougeText[3].text = "똥이 들어가버렸다.";
                    uiControl.MinusHealth(5.0f);
                    uiControl.MinusMental(5.0f);
                    waitNextDay = true;
                }
                break;
            case 111300:
                choiceDialougeText[3].text = "여기서 쌀까?";
                break;
            case 111301:
                Percent = ReturnPercent();
                if (Percent <= 50)
                {
                    choice[0].gameObject.SetActive(false);
                    choice[1].gameObject.SetActive(false);
                    choice[2].gameObject.SetActive(false);
                    choiceDialougeText[0].text = "";
                    choiceDialougeText[1].text = "";
                    choiceDialougeText[3].text = "으아악! 똥위로 미끄러져 버렸다.";
                    uiControl.MinusMental(10.0f);
                    waitNextDay = true;

                }
                else if (Percent > 50)
                {
                    choice[0].gameObject.SetActive(false);
                    choice[1].gameObject.SetActive(false);
                    choice[2].gameObject.SetActive(false);
             
                    choiceDialougeText[3].text = "시원하게 마무리했다!";
                    uiControl.PlusMental(5.0f);
                    waitNextDay = true;
                }
                break;
            case 111302:
                Percent = ReturnPercent();
                if (Percent <= 50)
                {
                    choice[0].gameObject.SetActive(false);
                    choice[1].gameObject.SetActive(false);
                    choice[2].gameObject.SetActive(false);
                    choiceDialougeText[0].text = "";
                    choiceDialougeText[1].text = "";
                    choiceDialougeText[3].text = "아름다운 꽃밭이다. 여기로 정했다!";
                    uiControl.PlusHealth(8.0f);
                    uiControl.PlusMental(8.0f);
                    uiControl.PlusSupply(8.0f);
                    waitNextDay = true;

                }
                else if (Percent > 50 && Percent <= 80)
                {
                    dialouge = 111100;
                    SelectDialgoue();
                    Dialouge();
                }
                else if (Percent > 80)
                {
                    choice[0].gameObject.SetActive(false);
                    choice[1].gameObject.SetActive(false);
                    choice[2].gameObject.SetActive(false);
                    choiceDialougeText[0].text = "";
                    choiceDialougeText[1].text = "";
                    choiceDialougeText[3].text = "똥이 들어가버렸다.";
                    uiControl.MinusHealth(5.0f);
                    uiControl.MinusMental(5.0f);
                    waitNextDay = true;
                }
                break;
            case 112000:
                choiceDialougeText[3].text = "원주민이다. 무언가를 팔려고 하는 것 같다.";
                break;
            case 112001:
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[0].text = "";
                choiceDialougeText[1].text = "";

                choiceDialougeText[3].text = "원주민은 대신 의약품을 가져갔다.";
                uiControl.PlusSupply(10.0f);
                uiControl.MinusHealth(10.0f);
                waitNextDay = true;
                break;
            case 112002:
                choice[0].gameObject.SetActive(false);
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                choiceDialougeText[0].text = "";
                choiceDialougeText[1].text = "";

                choiceDialougeText[3].text = "원주민은 대신 보급품을 가져갔다.";
                uiControl.PlusHealth(10.0f);
                uiControl.MinusSupply(10.0f);
                waitNextDay = true;
                break;
            default:
                break;


        }

    }

  
    public void SelectDialgoue() //선택지 
    {
        Debug.Log("아래쪽 다이얼로그");
        Debug.Log(dialouge);
        switch(dialouge)                                                                                                                                        
        {
            case 101000:
                choiceDialougeText[0].text = "열자!";
                choiceDialougeText[1].text = "열지 말자";
                choice[2].gameObject.SetActive(false);
                break;
            case 102000:
                choiceDialougeText[0].text = "보석인가!?";
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);   
                break;
            case 103000:
                choiceDialougeText[0].text = "꽃향기가 나는 방향으로 간다.";
                choiceDialougeText[1].text = "반대로 이동한다.";
                choice[2].gameObject.SetActive(false);
        
                break;
            case 103100:
                choiceDialougeText[0].text = "음~ 한번만 핥아볼까?";
                choiceDialougeText[1].text = "건드리면 안될거 같군";
                choice[2].gameObject.SetActive(false);
                break;
            case 104000:
                choiceDialougeText[0].text = "흠.. 그냥 먹지뭐";
                choiceDialougeText[1].text = "이건 버려야 겠군..";
                choice[2].gameObject.SetActive(false);
                break;
            case 105000:
                choiceDialougeText[0].text = "그럼 벌레를 먹으면 되지!";
                choiceDialougeText[1].text = "벌레를 제거한다.";
                choice[2].gameObject.SetActive(false);
                break;
            case 106000:
                choiceDialougeText[0].text = "그냥 지나간다.";
                choiceDialougeText[1].text = "돌아서 간다.";
                choice[2].gameObject.SetActive(false);
                break;
            case 107000:
                choiceDialougeText[0].text = "오늘 저녁은 고기다!";
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                break;
            case 108000:
                choiceDialougeText[0].text = "쉬었다 가자!";
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                break;
            case 109000:
                choiceDialougeText[0].text = "역시 자연은 위대해!";
                choice[1].gameObject.SetActive(false);
                choice[2].gameObject.SetActive(false);
                break;
            case 110000:
                choiceDialougeText[0].text = "자~ 이리온";
                choiceDialougeText[1].text = "썩 물러가라 이놈들!";
                choice[2].gameObject.SetActive(false);
                break;
            case 111000:
                choiceDialougeText[0].text = "바지를 내리고 똥을 싼다.";
                choiceDialougeText[1].text = "똥을 쌀만한 장소를 찾아보자";
                choice[2].gameObject.SetActive(false);
                break;
            case 111100:
                choiceDialougeText[0].text = "바람이 부는 방향으로 가자!";
                choiceDialougeText[1].text = "물소리가 나는 방향으로 가자!";
                choice[2].gameObject.SetActive(false);
                break;
            case 111200:
                choiceDialougeText[0].text = "가즈아!";
                choiceDialougeText[1].text = "좀 더 가볼까?";
                choice[2].gameObject.SetActive(false);
                break;
            case 111300:
                choiceDialougeText[0].text = "가즈아!";
                choiceDialougeText[1].text = "좀 더 가볼까?";
                choice[2].gameObject.SetActive(false);
                break;
            case 112000:
                choiceDialougeText[0].text = "보급! 보급을 내놔라!";
                choiceDialougeText[1].text = "약으로 쓸만한걸 줘라!";
                choice[2].gameObject.SetActive(false);
                break;
           
            default:
                break;

        }


        
    }


    public void ButtonTest1()
    {
        Debug.Log("1눌렸음");
        dialouge++;
        Dialouge();
    }
    public void ButtonTest2()
    {
        Debug.Log("2눌렸음");
        dialouge += 2;
        Dialouge();
    }
    public void ButtonTest3()
    {
        Debug.Log("3눌렸음");
        dialouge += 3;
        Dialouge();
    }

}
