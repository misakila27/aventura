﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainSceneButton : MonoBehaviour
{
    public GameObject storePanel;
    public GameObject mainPanel;

    public GameObject optionBar;
    public GameObject achieveScrollView;
    public GameObject treasureScrollView;

    public Toggle soundToggle;
    public Toggle musicToggle;
    public Toggle achieveToggle;

    public void StartButton()
    {

        if(GameObject.Find("ResourceLoad") == null)
        {
            SceneManager.LoadScene("CharacterSelectScene");
        }
        else
            SceneManager.LoadScene("InGameScene");
    }

    public void OptionButton()
    {
        if (optionBar.activeSelf == false)
        {
            optionBar.SetActive(true);
        }
        else
        {
            optionBar.SetActive(false);
        }

    }

    public void SoundToggle()
    {
        if (soundToggle.isOn)
        {

        }
        else
        {

        }
    }

    public void MusicToggle()
    {
        if (musicToggle.isOn)
        {

        }
        else
        {

        }
    }

    public void StoreButton()
    {
        storePanel.SetActive(true);
        mainPanel.SetActive(false);
    }

    public void StoreExitButton()
    {
        storePanel.SetActive(false);
        mainPanel.SetActive(true);
    }

    public void Achieve()
    {
     
            achieveScrollView.SetActive(true);
            treasureScrollView.SetActive(false);

      
    }

    public void Treasure()
    {
        achieveScrollView.SetActive(false);
        treasureScrollView.SetActive(true);
    }   
}
