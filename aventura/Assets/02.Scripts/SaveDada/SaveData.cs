﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class SaveData : MonoBehaviour
{

    public string userName;
    public bool music;
    public bool sound;
    public int coin;
    public float bestScore;
    public int longestDay;




    public static SaveData Instance;


    private void Awake()
    {
        if (Instance == null)
        {
            Debug.LogError("istance 가 없음 ");
        }
        Instance = this;


        if (System.IO.File.Exists(Path.Combine(Application.dataPath, "saveData.json")))
        {
            string saveDataPath = Path.Combine(Application.dataPath, "saveData.json");
            string jsonData = File.ReadAllText(saveDataPath);
            JsonUtility.FromJsonOverwrite(jsonData, Instance);
        }
    }

       


// Start is called before the first frame update
void Start()
    {
            
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
