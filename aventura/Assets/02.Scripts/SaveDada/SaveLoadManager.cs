﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

//// utf-8 인코딩

//byte[] bytesForEncoding = Encoding.UTF8.GetBytes(인코딩 할 유니코드 string 변수);

//string encodedString = Convert.ToBase64String(bytesForEncoding);



//// utf-8 디코딩

//byte[] decodedBytes = Convert.FromBase64String(encodedString);

//string decodedString = Encoding.UTF8.GetString(decodedBytes);


public class SaveLoadManager : MonoBehaviour
{
   private static SaveLoadManager _instance = null;

    public static SaveLoadManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new SaveLoadManager();
             
            }
            return _instance;
        }
    }


    #region



    #endregion

    void Start()
    {
        LoadData();
        
    }

    public void PlustempCoin()
    {
        TempSaveData.Instance.coin += 1;
    }


    public void OnGameSave() //게임 진행 도중 Save 1
    {
        Debug.Log("OnGameSave");
        string jsonData = JsonUtility.ToJson(OnGameSaveData.Instance, true);
        string path = Path.Combine(Application.dataPath, "onGameSaveData.json");
        File.WriteAllText(path, jsonData);

    }

    public void ArriveBase() //거점에 도착하여 게임 종료 시 
    {
        string jsonData = JsonUtility.ToJson(TempSaveData.Instance, true);
        string path = Path.Combine(Application.dataPath, "tempSaveData.json");
        File.WriteAllText(path, jsonData);

        OverWriteSaveData();
        SaveData.Instance.userName = EncodingUTF8(SaveData.Instance.userName);
        string saveJsonData = JsonUtility.ToJson(SaveData.Instance, true);
        string saveDatapath = Path.Combine(Application.dataPath, "saveData.json");
        File.WriteAllText(saveDatapath, saveJsonData);

        System.IO.File.Delete(Path.Combine(Application.dataPath, "onGameSaveData.json"));
        System.IO.File.Delete(Path.Combine(Application.dataPath, "onGameSaveData.json.meta"));

    }

    public void OnGameLoadData() //게임 진행 도중 로드(이어하기) 
    {
        Debug.Log("OnGameLoad");
        string path = Path.Combine(Application.dataPath, "onGameSaveData.json");
        string jsonData = File.ReadAllText(path);
        JsonUtility.FromJsonOverwrite(jsonData, OnGameSaveData.Instance);
    }

    public void LoadData() //게임 시작 시 정보를 가져옴
    {



    }


    void OverWriteSaveData()
    {
        if(TempSaveData.Instance.music != SaveData.Instance.music)
        {
            SaveData.Instance.music = TempSaveData.Instance.music;
        }
        if (TempSaveData.Instance.sound != SaveData.Instance.sound)
        {
            SaveData.Instance.sound = TempSaveData.Instance.sound;
        }

        if(TempSaveData.Instance.bestScore > SaveData.Instance.bestScore)
        {
            SaveData.Instance.bestScore = TempSaveData.Instance.bestScore;
        }


        SaveData.Instance.coin = TempSaveData.Instance.coin;


    }

    public static string EncodingUTF8(string name)
    {
       

        byte[] bytesForEncoding = System.Text.Encoding.UTF8.GetBytes(name);
        string encodedString = System.Convert.ToBase64String(bytesForEncoding);
        return encodedString;
    }

    public static string DecodingUTF8(string name)
    {
        //byte[] decodedBytes = Convert.FromBase64String(encodedString);

        //string decodedString = Encoding.UTF8.GetString(decodedBytes);

        byte[] bytesForDecoding = System.Convert.FromBase64String(name);
        string decodeString = System.Text.Encoding.UTF8.GetString(bytesForDecoding);
        return decodeString;
    }

}

