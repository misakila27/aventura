﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempSaveData : MonoBehaviour
{

    public List<int> archiveID;
    public List<int> artifactID;
    public bool music;
    public bool sound;
    public int coin;
    public float bestScore;
    public int mainStoryID;

    // Start is called before the first frame update


    private static TempSaveData _instance = null;

    public static TempSaveData Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType(typeof(TempSaveData)) as TempSaveData;
                if (_instance == null)
                {
                    Debug.LogError("istance 가 없음 ");
                }
            }
            return _instance;
        }
    }

    void Start()
    { }

    void Update()
    {

    }
   


}